package com.catch22.pong.tools;

import com.catch22.pong.game.Pong;

import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Customize extends JFrame implements ActionListener
{
	private static final long serialVersionUID = 1L;
	
	private JLabel heightLabel, widthLabel, speedLimitLabel;
	private JTextField heightField, widthField, speedLimitField;
	private JButton speedLimitToggleUp, speedLimitToggleDown, themeDark, themeLight, Done;
	
	public Customize()
	{
		final int horizontalOffset = 140, verticalOffset = 60;
		final int fieldHeight = 30;
		final int rowSpacing = 10;
		
		heightLabel = new JLabel("Height:");
		widthLabel = new JLabel("Width:");
		speedLimitLabel = new JLabel("Speed Limit:");
		heightField = new JTextField(String.valueOf(Pong.height));
		widthField = new JTextField(String.valueOf(Pong.width));
		speedLimitField = new JTextField(String.valueOf(5));

		speedLimitToggleUp = new JButton("↑");
		speedLimitToggleDown = new JButton("↓");
		
		themeLight = new JButton("Light");
		themeDark = new JButton("Dark");
		
		if(Pong.theme)
			themeDark.setText(themeDark.getText() + " ✓");
		else
			themeLight.setText(themeLight.getText() + " ✓");
		
		Done= new JButton("Done");
		
		add(speedLimitToggleUp);
		speedLimitToggleUp.setFont(new Font("Arial", 10, 10));
		speedLimitToggleUp.setBounds(horizontalOffset - 10 + 90 + 50, verticalOffset + 2*fieldHeight + 2*rowSpacing, 20, fieldHeight / 2);
		speedLimitToggleUp.setMargin(new Insets(4, 2, 5, 2));
		add(speedLimitToggleDown);
		speedLimitToggleDown.setFont(new Font("Arial", 10, 10));
		speedLimitToggleDown.setBounds(horizontalOffset - 10 + 90 + 50, verticalOffset + fieldHeight / 2 + 2*fieldHeight + 2*rowSpacing, 20, fieldHeight / 2);
		speedLimitToggleDown.setMargin(new Insets(4, 2, 5, 2));
		
		add(heightField);
		heightField.setBounds(horizontalOffset + 90, verticalOffset, 50, fieldHeight);
		add(widthField);
		widthField.setBounds(horizontalOffset + 90, verticalOffset + fieldHeight + rowSpacing, 50, fieldHeight);
		add(speedLimitField);
		speedLimitField.setBounds(horizontalOffset - 10 + 90, verticalOffset + 2*fieldHeight + 2*rowSpacing, 50, fieldHeight);
		add(heightLabel);
		heightLabel.setBounds(horizontalOffset, verticalOffset, 80, fieldHeight);
		add(widthLabel);
		widthLabel.setBounds(horizontalOffset, verticalOffset + fieldHeight + rowSpacing, 80, fieldHeight);
		add(speedLimitLabel);
		speedLimitLabel.setBounds(horizontalOffset - 20, verticalOffset + 2*fieldHeight + 2*rowSpacing, 100, fieldHeight);
		
		add(themeDark);
		themeDark.setBounds(horizontalOffset + 20 - 20, verticalOffset + 140, 90, 30);
		add(themeLight);
		themeLight.setBounds(horizontalOffset + 20 + 60, verticalOffset + 140, 90, 30);
		
		add(Done);
		Done.setBounds(horizontalOffset + 20 + 30, verticalOffset + 200, 90, 30);
		
		add(new JLabel());
		
		speedLimitToggleUp.addActionListener(new SpeedLimitToggled(1));
		speedLimitToggleDown.addActionListener(new SpeedLimitToggled(-1));

		themeDark.addActionListener(new ThemeSelected(true));
		themeLight.addActionListener(new ThemeSelected(false));
		
		Done.addActionListener(this);
		
		setTitle("Customize game");
		setSize(450, 400);
		setVisible(true);
	}
	
	private class SpeedLimitToggled implements ActionListener
	{
		private int delta;
		
		public SpeedLimitToggled(int change)
		{
			delta = change;
		}
		
		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			speedLimitField.setText(String.valueOf(Integer.valueOf(speedLimitField.getText()) + delta));			
		}		
	}
	
	private class ThemeSelected implements ActionListener
	{
		private boolean selectedTheme;
		
		public ThemeSelected(boolean selected)
		{
			selectedTheme = selected;
		}
		
		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			if(selectedTheme)
			{
				themeDark.setText("Dark ✓");
				themeLight.setText("Light");
			}
			
			else
			{
				themeDark.setText("Dark");
				themeLight.setText("Light ✓");
			}
			Pong.theme = selectedTheme;
		}
		
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0)
	{
		Pong.width = Integer.valueOf(widthField.getText());
		Pong.height = Integer.valueOf(widthField.getText());
		Pong.speedLimit = 6 - Integer.valueOf(speedLimitField.getText());
		this.dispose();	
	}	
	
//	public static void main(String[] args)
//	{
//		new Customize();
//	}
}